from urllib.request import urlopen
import datetime
from datetime import datetime
from math import radians, cos, sin, asin, sqrt
from glob import glob
import numpy as np
from sklearn.cluster import AgglomerativeClustering 
from .wtputils import *

#------------------------------------------------------------------------------
# finding areas where > min_group_vehs buses exceed speed (50)
def find_bus_groups(buses_db, distance: float, min_velocity: float, min_group_vehs: int):
    """
    Function that computes and find bus groups of buses that exceed speed
    Parameters:
        buses_db: database of buses got from function build_buses_database from module data_compute,
        distance (float): distance in meters for buses in a group
        min_velocity (float): minimum velocity needed to take bus into account
        min_group_vehs (int): minimum nubmer of vehicles in a group to take this group into account

    Returns:
        dict: with group label as a key (int) and group parameters as value 
    """
    one_dg_lat = 1850*60
    one_dg_lon = 1850*60*cos(52.229676)
    ext_degree=1/6000
    bus_groups={}
    p = [r for v in buses_db.values() for r in v if r['velocity'] > min_velocity]
    X = [[one_dg_lat*r['Lat'], one_dg_lon*r['Lon']] for r in p]
    c = AgglomerativeClustering(None, distance_threshold=distance, linkage='single')
    gs=c.fit(X)
    for i,g in enumerate(gs.labels_):
        try:
            bus_groups[str(g)].append(p[i])
        except:
            bus_groups[str(g)] = [p[i]]
    for k,v in bus_groups.items():
        min_lat = v[0]['Lat']
        max_lat = v[0]['Lat']
        min_lon = v[0]['Lon']
        max_lon = v[0]['Lon']
        v_numbers = set()
        for r in v:
            min_lat = min(min_lat, r['Lat'])
            max_lat = max(max_lat, r['Lat'])
            min_lon = min(min_lon, r['Lon'])
            max_lon = max(max_lon, r['Lon'])
            v_numbers.add(r['Veh nr'])
        bus_groups[k] = {
            'lat': [min_lat-ext_degree,max_lat+ext_degree],
            'lon': [min_lon-ext_degree,max_lon+ext_degree],
            'vehs': list(v_numbers),
            'overspeed nr': len(v)
        }
    for k in list(bus_groups.keys()):
        if len(bus_groups[k]['vehs']) < min_group_vehs:
            del(bus_groups[k])
    return bus_groups
#------------------------------------------------------------------------------
# function calculating percent of overspeed acts
def calc_percent_of_overspd(buses_database, bus_group):
    """
    Function that add to bus_groups variable certain fields with statistical information
    Parameters:
        buses_database: database of buses and results over time
        bus_group: dictionary with group label as a key (int) and group parameters as value 

    Returns:
        dict: extended dict of busgroups db of 'all acts' and 'percent' field
    """

    for rect in bus_group.values():
        if 'all acts' in rect:
            continue
        rect['all acts'] = 0
        for bus in buses_database.values():
            #check if bus is in group then add or start counting
            for result in bus:
                #print('res:',result)
                res_lons = result['Lon']
                res_lats = result['Lat']
                if min(rect['lon'])< res_lons < max(rect['lon']) and min(rect['lat']) < res_lats < max(rect['lat']):
                    rect['all acts']+=1   
    for rect in bus_group.values():
        rect['percent'] = rect['overspeed nr']/rect['all acts']*100
    return bus_group
#------------------------------------------------------------------------------
# function returning regions where significant percent of acts of overspeed
def regions_signif_ovspd(bus_group, percent: float):
    """
    Function that returns regions where significant percent of buses exceed speed
    Parameters:
        bus_group: dictionary with group label as a key (int) and group parameters as value
        percent (float): minimum percent of buses in group exceeding speed
    Returns:
        dict: regions where significant percent of buses exceed speed
    """
    return {k:v for k,v in bus_group.items() if v['percent']>percent}
#------------------------------------------------------------------------------
# function calculating average bus speed over whole period, can be restricted to buses that are in at least releveance_percent of data 
def avg_busspeed_per_bus(buses_database, relevance_percent: float):
    """
    Function that compute average speed for each bus
    Parameters:
        buses_database: database of buses and results over time
        relevance_percent (float): minimum percent of results over time for a bus to be taken into account
    Returns:
        dict: with bus number as a key and its average speed as a avalue
    """
    buses_avg_speed={}
    max_nr_res_bus = 0
    cur_nr_res_bus = 0
    for bus in buses_database.values():
        for result in bus:
            if result != None:
                cur_nr_res_bus+=1
        max_nr_res_bus=max(cur_nr_res_bus,max_nr_res_bus)
        cur_nr_res_bus=0
    for k, bus in buses_database.items():
        avg_busspeed=0
        sum=0
        if len(bus)>=relevance_percent/100 * max_nr_res_bus:
            for i in bus:
                sum=sum + i['velocity']
            avg_busspeed = sum/len(bus)
            buses_avg_speed[k]={
                'line number': bus[0]['Line nr'],
                'avg speed': avg_busspeed
            }
    return buses_avg_speed
#------------------------------------------------------------------------------
# function calculating avg busspeed for whole buses
def avg_busspeed(buses_avg_speed):
    """
    Function that compute average speed for all buses
    Parameters:
       buses_avg_speed: dictionary with keys as a bus numbers and avg speed as values 
    Returns:
        float: avg bus speed
    """
    sum=0
    for bus in buses_avg_speed.values():
        sum = sum + bus['avg speed']
    if len(buses_avg_speed) ==0:
        return None
    return sum/len(buses_avg_speed)
#------------------------------------------------------------------------------
# function calculating punctuality of buses - time diff between scheduled and real arrival
def bus_on_stop(buses_db, line_schedules, distance_diff: float, max_speed:float):
    """
    Function that computes difference between scheduled and arrival time for a bus on an appriopriate bus_stop
    Parameters:
        buses_db: database of buses and results over time
        line_schedules: database of schedules for lines with busstops
        distance_diff (float): max distance between a bus and a bus stop to consider that bus stays on a stop (in meters)
        max_speed (float): max velocity of a bus to consider that bus stays on a stop (in km/h)
    Returns:
        list of dicts: with bus number as a key and bus info as a value
    """
    bus_stop_meets=[]
    for k, v in buses_db.items():
        for bus_result in v:
            if bus_result['velocity']>max_speed:
                continue
            try:
                for schedule in line_schedules[bus_result['Line nr']]:
                    try:
                        dist = dist_calc(bus_result['Lon'], float(schedule['Lon']), bus_result['Lat'], float(schedule['Lat']))
                        if dist<distance_diff:
                            best_one_stop = None
                            best_time_diff = 1e6
                            for one_stop in schedule['rozklad']:
                                try:
                                    time_from_schedule = datetime.strptime(one_stop,'%H:%M:%S')
                                except:
                                    tmp = str(float(one_stop[:2]) %24)
                                    proper_time = tmp[0:1] + one_stop[2:]
                                    time_from_schedule = datetime.strptime(proper_time,'%H:%M:%S')
                                    # print(proper_time, one_stop, bus_result['Line nr'])
                                time_from_bus = datetime.strptime(bus_result['Time'],'%Y-%m-%d %H:%M:%S')
                                time_from_bus=time_from_bus.replace(year=1900,month=1,day=1)
                                time_diff = abs((time_from_bus-time_from_schedule).total_seconds())
                                if time_diff < best_time_diff:
                                    best_time_diff = time_diff
                                    best_one_stop = one_stop
                            if best_one_stop is not None:
                                bus_stop_meets.append({
                                    'bus nr' : k,
                                    'line nr' : bus_result['Line nr'],
                                    'stop_id' : schedule['stop_id'],
                                    'slupek_id' : schedule['slupek_id'],
                                    # add rest
                                    'time arrival' : bus_result['Time'],
                                    'schedule time' : best_one_stop,
                                    'time diff' : best_time_diff,
                                    'dist' : dist,
                                    'velocity' : bus_result['velocity']
                                })      
                    except:
                        pass
            except KeyError:
                raise Exception(f"Inconsistent data between buses_db and line_schedules, missing Schedule for line number: {bus_result['Line nr']}")                
    return bus_stop_meets
#------------------------------------------------------------------------------
# statistics calculations for punctuality of buses
def bus_on_stop_stats(bus_on_stop_db):
    """
    Function that computes statistics for punctuality of buses
    Parameters:
        bus_on_stop_db (list of dicts): with bus number as a key and bus info as a value
    Returns:
        dict: statistics for buses
    """
    tmp= [r['time diff'] for r in bus_on_stop_db]
    return {
        'mean': np.mean(tmp),
        'std': np.std(tmp),
        'min': np.min(tmp),
        'max': np.max(tmp)
    }
#------------------------------------------------------------------------------
# function computing amount of overspeed acts during whole period
def overspeed_acts(buses_db, velocity):
    """
    Function that computes number of overspeed acts over a specified time
    Parameters:
        buses_db: database of buses and results over a specified time
    Returns:
        dict: with bus number as a key and number of overspeed acts as a value
    """
    buses_ovspd={}
    for k, v in buses_db.items():
        for result in v:
            if result['velocity']>=velocity:
                try:
                    buses_ovspd[k]+=1
                except:
                    buses_ovspd[k]=1
    return buses_ovspd

def overspeed_buses(buses_ovspd, number_of_exceeds):
    """
    Function that computes number of buses that exceed speed
    Parameters:
        buses_ovspd(dict): with bus number as a key and number of overspeed acts as a value
        number_of_exceeds: minimum number of speed exceeds for a bus to be taken as overspeeding one
    Returns:
        int: number of buses that exceed speed
    """
    for bus in list(buses_ovspd.keys()):
        if buses_ovspd[bus] < number_of_exceeds:
            del buses_ovspd[bus]
    return len(buses_ovspd)