import json

# creating files to use on geojson.io

#------------------------------------------------------------------------------
# drawing bus positions
def create_geojson_file_bus_positions(buses_database, number_of_sample: int, min_velocity: float, path_to_export: str):
    """
    Function that creates file for site geojson.io with a bus positions over a specified time
    Parameters:
        buses_database: database of buses and results over time
        number_of_sample: number of result that will be printed
        min_velocity (float): minimum velocity to draw a pin with a red colour
        path_to_export (str): path with a name of a file that will be generated
    Returns:
        nothing
    """
    result_geojson={}
    result_geojson["type"] = "FeatureCollection"
    result_geojson["features"] = []
    for d in buses_database.values():
        for r in d:
            if r['sample'] == number_of_sample:
                ficzer={}
                ficzer["type"] = "Feature"
                ficzer["properties"]={
                    "marker-color": "#de5e5e" if r['velocity'] > min_velocity else "#27f011",
                    "marker-size": "medium",
                    "marker-symbol": "",
                    "bus_id": r['Veh nr'],
                    "line number": r['Line nr'],
                    "velocity": r['velocity']
                }
                ficzer["geometry"]={
                    "type": "Point",
                    "coordinates": [
                        r['Lon'],
                        r['Lat']
                    ]
                }
                result_geojson["features"].append(ficzer)
    with open(path_to_export, 'w') as outfile:
        json.dump(result_geojson, outfile)
#------------------------------------------------------------------------------
# drawing bus way for one time interval
def create_geojson_file_bus_way_one_sample(buses_database, number_of_sample: int, min_velocity: float,path_to_export: str):
    """
    Function that creates file for site geojson.io with a bus ways over a specified time
    Parameters:
        buses_database: database of buses and results over time
        number_of_sample (int): number of result that will be printed
        min_velocity (float): minimum velocity to draw a pin with a red colour
        path_to_export (str): path with a name of a file that will be generated
    Returns:
        nothing
    """
    result_geojson={}
    result_geojson["type"] = "FeatureCollection"
    result_geojson["features"] = []
    for d in buses_database.values():
        for r in d:
            if r['sample'] == number_of_sample:
                ficzer={}
                ficzer["type"] = "Feature"
                ficzer["properties"]={
                    "stroke": "#de5e5e" if r['velocity'] > min_velocity else "#27f011",
                    "stroke-width": 2,
                    "stroke-opacity": 1,
                    "bus_id": r['Veh nr'],
                    "line number": r['Line nr'],
                    "velocity": r['velocity']
                }
                ficzer["geometry"]={
                    "type": "LineString",
                    "coordinates": [
                        [
                            r['Lons'][0],
                            r['Lats'][0]
                        ],
                        [
                            r['Lons'][1],
                            r['Lats'][1]
                        ]
                    ]
                }
                result_geojson["features"].append(ficzer)
    with open(path_to_export, 'w') as outfile:
        json.dump(result_geojson, outfile)
#------------------------------------------------------------------------------
# drawing whole bus way for one bus
def create_geojson_file_bus_way(buses_database, number_of_veh: str, min_velocity: float,path_to_export: str):
    """
    Function that creates file for site geojson.io with a bus way for whole time
    Parameters:
        buses_database: database of buses and results over time
        number_of_veh (str): number of vehicle
        min_velocity (float): minimum velocity to draw a pin with a red colour
        path_to_export (str): path with a name of a file that will be generated
    Returns:
        nothing
    """
    result_geojson={}
    result_geojson["type"] = "FeatureCollection"
    result_geojson["features"] = []
    for d in buses_database.values():
        for r in d:
            if r['Veh nr'] == number_of_veh:
                ficzer={}
                ficzer["type"] = "Feature"
                ficzer["properties"]={
                    "stroke": "#de5e5e" if r['velocity'] > min_velocity else "#27f011",
                    "stroke-width": 2,
                    "stroke-opacity": 1,
                    "bus_id": r['Veh nr'],
                    "line number": r['Line nr'],
                    "velocity": r['velocity'],
                    "sample": r['sample']
                }
                ficzer["geometry"]={
                    "type": "LineString",
                    "coordinates": [
                        [
                            r['Lons'][0],
                            r['Lats'][0]
                        ],
                        [
                            r['Lons'][1],
                            r['Lats'][1]
                        ]
                    ]
                }
                result_geojson["features"].append(ficzer)
    with open(path_to_export, 'w') as outfile:
        json.dump(result_geojson, outfile)
#------------------------------------------------------------------------------
# drawing whole bus way for all buses
def create_geojson_file_bus_whole_way(buses_database, min_velocity, path_to_export):
    """
    Function that creates file for site geojson.io with a buses ways for a whole time
    Parameters:
        buses_database: database of buses and results over time
        min_velocity (float): minimum velocity to draw a pin with a red colour
        path_to_export (str): path with a name of a file that will be generated
    Returns:
        nothing
    """
    result_geojson={}
    result_geojson["type"] = "FeatureCollection"
    result_geojson["features"] = []
    for d in buses_database.values():
        for r in d:
            ficzer={}
            ficzer["type"] = "Feature"
            ficzer["properties"]={
                "stroke": "#de5e5e" if r['velocity'] > min_velocity else "#27f011",
                "stroke-width": 2,
                "stroke-opacity": 1,
                "bus_id": r['Veh nr'],
                "line number": r['Line nr'],
                "velocity": r['velocity'],
                "sample": r['sample']
            }
            ficzer["geometry"]={
                "type": "LineString",
                "coordinates": [
                    [
                        r['Lons'][0],
                        r['Lats'][0]
                    ],
                    [
                        r['Lons'][1],
                        r['Lats'][1]
                    ]
                ]
            }
            result_geojson["features"].append(ficzer)
    try:
        with open(path_to_export, 'w') as outfile:
            json.dump(result_geojson, outfile)
    except:
        pass
#------------------------------------------------------------------------------
# drawing rectangles where many buses exceed velocity
def create_geojson_file_rectangle(bus_groups,path_to_export):
    """
    Function that creates file for site geojson.io with areas of buses where many buses exceeds speed,
    based on find_bus_groups function in data_compute module
    Parameters:
        bus_groups: database of results to be printed
        path_to_export (str): path with a name of a file that will be generated
    Returns:
        nothing
    """
    result_geojson={}
    result_geojson["type"] = "FeatureCollection"
    result_geojson["features"] = []
    for g,v in bus_groups.items():
        ficzer={}
        ficzer["type"] = "Feature"
        ficzer["properties"]={
            "group": str(g),
            "fill": "#E60808"
        }
        ficzer["geometry"]={
            "type": "Polygon",
            "coordinates": [
                [
                    [v['lon'][0],v['lat'][0]],
                    [v['lon'][1],v['lat'][0]],
                    [v['lon'][1],v['lat'][1]],
                    [v['lon'][0],v['lat'][1]],
                ]
            ]
        }
        result_geojson["features"].append(ficzer)
    with open(path_to_export, 'w') as outfile:
        json.dump(result_geojson, outfile) 