from math import radians, cos, sin, asin, sqrt
import json
#------------------------------------------------------------------------------
# functions to calculate distance and velocity from gps coords
def dist_calc(lon1: float,lon2: float,lat1: float,lat2: float):
    """
    Function that calculates distance between two points on Earth

    Parameters:
        lon1 (float): longitude of first point
        lon2 (float): longitude of second point
        lat1 (float): latitude of first point
        lat2 (float): latitude of second point
    
    Returns:
        float: distance between points in km
    """
    R=6371
    lon1=radians(lon1)
    lon2=radians(lon2)
    lat1=radians(lat1)
    lat2=radians(lat2)
    haversine_formula = sin((lat2-lat1) / 2)**2 + cos(lat1) * cos(lat2) * sin((lon2-lon1) / 2)**2
    distance = 2*asin(sqrt(haversine_formula))*R
    return distance
#------------------------------------------------------------------------------
def dist_calc_kmh(lon1: float,lon2: float,lat1: float,lat2: float,time: float):
    """
    Function that calculates distance traveled by bus and speed of bus over time period

    Parameters:
        lon1 (float): longitude of first point
        lon2 (float): longitude of second point
        lat1 (float): latitude of first point
        lat2 (float): latitude of second point
        time (float): time difference (in seconds) between two notifications of bus
    
    Returns:
        (float, float): distance traveled by bus in km, velocity of bus in km/h
    """
    distance = dist_calc(lon1,lon2,lat1,lat2)
    velocity = 3600* distance/time
    return (distance, velocity)
#------------------------------------------------------------------------------
def load_data(path: str):
    """
    Function that loads data from a file from a specified path

    Parameters:
        path (str): path to a file which will be loaded
    
    Returns:
        nothing
    """
    with open(path, "r") as f:
        return json.load(f)
#------------------------------------------------------------------------------
def save_data(database, path: str):
    """
    Function that saves data to a specified file

    Parameters:
        database: database variable
        path (str): path to a file which will be loaded
    
    Returns:
        nothing
    """
    with open(path, 'w') as f:
        json.dump(database, f)
#------------------------------------------------------------------------------