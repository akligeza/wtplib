import time
import os
import json
import threading, queue
from .wtpapi import *

#------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# buses postions requesting
def get_buses_dataset(apikey: str):
    """
    Function that get data of bus positions from api.um.warszawa for one sample

    Parameters:
        apikey (str): key needed to access data

    Returns:
        dict with vehicle number as key and all other vehicle data as value
    """
    res = get_buses_positions(apikey)
    return {i['VehicleNumber']:i for i in res['result']}

#------------------------------------------------------------------------------
# buses position saving to files
def download_buses_dataset(folder_path: str, apikey: str,snapshots_number: int,interval_time: int,*,tries_number: int =10):
    """
    Function that get data of bus positions from api.um.warszawa for a certain number of samples
    and with a specified interval_time between samples, saves data to a specified folder as files
    with proper logical generated names: buses_positions_{number of sample}.json

    Parameters:
        folder_path (str): path where you want to save your data,
        apikey (str): key needed to access data,
        snapshots_number (int): number of samples that you want to get
        interval_time (int): time in seconds between two samples
        tries_number (int): max number of repeated tries when something went wrong and data couldn't be collected

    Returns:
        nothing
    """
    try:
        os.mkdir(folder_path)
    except:
        pass
    for i in range(snapshots_number):
        result = None
        for _ in range(tries_number):
            try:
                result = get_buses_dataset(apikey)
                with open(os.path.join(folder_path, f'buses_positions_{i}.json'), 'w') as outfile:
                    json.dump(result, outfile)
                if i+1 != snapshots_number:
                    time.sleep(interval_time)
                break
            except:
                time.sleep(1)
        if result is None:
            raise Exception(f"Unable to download full buses dataset. Stop on {i} snapshot.")
#------------------------------------------------------------------------------
# busstops positions requesting
def get_busstops_info(apikey: str):
    """
    Function that get data of one bus stop from api.um.warszawa

    Parameters:
        apikey (str): key needed to access data

    Returns:
        dict with bus stop info
    """
    response = get_busstops(apikey)
    return response['result']
#------------------------------------------------------------------------------
# busstops positions saving to a file
def download_busstops_info(file_name: str, apikey: str):
    """
    Function that get data of one bus stop from api.um.warszawa and save it into a file

    Parameters:
        apikey (str): key needed to access data
    Returns:
        dict with bus stop info
    """
    result = get_busstops_info(apikey)
    with open(file_name, 'w') as outfile:
        json.dump(result, outfile)
    return result
#------------------------------------------------------------------------------
# busstops schedule tables for bus lines saving to files using workers to increase data download speed
def download_busstops_schedules(folder_path: str, apikey: str, workers_number: int =5,*,tries_number: int =20, clbk_err=None, clbk_progress=None):
    """
    Function that get data for bus stop and their schedules saving them into a specified folder, making folder tree. Uses workers (as threads)
    to increase speed of downloading data due to slow repsonse of a server

    Parameters:
        folder_path (str): path where you want to save your data,
        apikey (str): key needed to access data,
        workers_number (int): number of workers that you want to use
        tries_number (int): max number of repeated tries when something went wrong and data couldn't be collected
        clbk_err: callback function for call in any significant error, got one argument with text description of error
        clbk_progress:
    """
    def try_until_success(fun, *args):
        for _ in range(tries_number):
            try:
                res=fun(*args)
                return res['result']
            except:
                time.sleep(6)
        raise Exception(f"Unable to get correct result in {tries_number} tries.")
    def worker(apikey,q,id):
        try:
            while True:
                stop_id, slupek_id = q.get_nowait()
                # try create folder for stop_id & slupek_id lines schedules
                os.makedirs(os.path.join(folder_path, stop_id, slupek_id), exist_ok=True)
                # check if all data is downloaded
                if os.path.isfile(os.path.join(folder_path, stop_id, slupek_id,'done.txt')):
                    continue
                try:
                    res = try_until_success(get_lines_for_busstop, apikey, stop_id, slupek_id)
                except:
                    if clbk_err is not None:
                        clbk_err(f'Unable to get lines for busstop stop_id {stop_id} and slupek_id {slupek_id}. Maybe try later!')
                    continue

                for j in res:
                    number=j['values'][0]['value']
                    os.makedirs(os.path.join(folder_path, stop_id, slupek_id, number), exist_ok=True)
                    try:
                        res2 = try_until_success(get_line_schedule_for_busstop, apikey, stop_id, slupek_id, number)
                    except:
                        if clbk_err is not None:
                            clbk_err(f'Unable to get schedule for line {number} for busstop stop_id {stop_id} and slupek_id {slupek_id}. Maybe try later!')
                        continue
                    with open(os.path.join(folder_path, stop_id, slupek_id, number,f'{number}.json'), 'w') as outfile:
                        json.dump(res2, outfile)
                # create done-marker file
                open(os.path.join(folder_path, stop_id, slupek_id,'done.txt'), 'a').close()
                if clbk_progress is not None:
                    clbk_progress(id, q.qsize())
        except queue.Empty:
            pass
    # creating jobs for workers and place them in queue
    q = queue.Queue()
    os.makedirs(folder_path, exist_ok=True)
    buses_stop_coords = download_busstops_info(os.path.join(folder_path, "busstops_info.json"), apikey)
    for i in buses_stop_coords:
        stop_id = i['values'][0]['value']
        slupek_id = i['values'][1]['value']
        q.put((stop_id,slupek_id))
    threads = [threading.Thread(target = worker, args=(apikey,q,id), daemon=True) for id in range(workers_number)]
    for t in threads:
        t.start()
    # wait for all workers done
    for t in threads:
        t.join()