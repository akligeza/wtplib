import requests
GET_TIMEOUT=10
def get_buses_positions(apikey: str):
    """
    Function that get data of bus positions from api.um.warszawa

    Parameters:
        apikey (str): key needed to access data
    
    Returns:
        json: file with bus positions
    """
    response = requests.get(f"https://api.um.warszawa.pl/api/action/busestrams_get/?resource_id=f2e5503e-927d-4ad3-9500-4ab9e55deb59&apikey={apikey}&type=1",timeout=GET_TIMEOUT)
    return response.json()

def get_busstops(apikey: str):
    """
    Function that get data of bus stops from api.um.warszawa
    
    Parameters:
        apikey (str): key needed to access data
    
    Returns:
        json: file with busstop info
    """
    response = requests.get(f"https://api.um.warszawa.pl/api/action/dbstore_get?id=ab75c33d-3a26-4342-b36a-6e5fef0a3ac3&apikey={apikey}",timeout=GET_TIMEOUT)
    return response.json()

def get_lines_for_busstop(apikey: str, stop_id: str, slupek_id: str):
    """
    Function that get data for lines that are on a specified busstop from api.um.warszawa
    
    Parameters:
        apikey (str): key needed to access data
    
    Returns:
        json: file with bus lines that are on a specified bus stop
    """
    response = requests.get(f"https://api.um.warszawa.pl/api/action/dbtimetable_get?id=88cd555f-6f31-43ca-9de4-66c479ad5942&busstopId={stop_id}&busstopNr={slupek_id}&apikey={apikey}",timeout=GET_TIMEOUT)
    return response.json()

def get_line_schedule_for_busstop(apikey: str, stop_id: str, slupek_id: str, line: str):
    """
    Function that get data of lines on specified busstop from api.um.warszawa
    
    Parameters:
        apikey (str): key needed to access data
    Returns:
        json: file with bus lines on a specified bus stop
    """
    response = requests.get(f"https://api.um.warszawa.pl/api/action/dbtimetable_get?id=e923fa0e-d96c-43f9-ae6e-60518c9f3238&busstopId={stop_id}&busstopNr={slupek_id}&line={line}&apikey={apikey}",timeout=GET_TIMEOUT)
    return response.json()