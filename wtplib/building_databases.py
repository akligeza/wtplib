import datetime
from datetime import datetime
import os
import json
from glob import glob
import numpy as np
from .wtputils import *

#------------------------------------------------------------------------------
# building busstops daatabase (busstop + its coords + its buslines schedules)
def build_busstops_database(folder_path):
    """
    Function that builds busstops database using data collected by the function download_busstops_schedules in collect_data module

    Parameters:
        folder_path (str): path where you want to save your database

    Returns:
        list: busstops database
    """
    busstops_database=[]
    with open(os.path.join(folder_path, "busstops_info.json"), "r") as f:
        data_coords=json.load(f)
    for dc in data_coords:
        dcv = dc['values']
        current_record ={
            "stop_id": dcv[0]['value'],
            "slupek_id": dcv[1]['value'],
            "nazwa_zespolu_przyst": dcv[2]['value'],
            "id_ulicy": dcv[3]['value'],
            "kierunek": dcv[6]['value'],
            "Lon": dcv[5]['value'],
            "Lat" :dcv[4]['value'],
            "rozklady": {}
        }
        busstop_lines = glob(os.path.join(folder_path, current_record['stop_id'], current_record['slupek_id'], '**', '*.json'))
        for record in busstop_lines:
            line_num = record.split(os.path.sep)[-2] # get line number from path - always second element from the end (folder name)
            with open(record, "r") as f:
                one_line_schedule = json.load(f)
            current_record["rozklady"][line_num]=[]
            for line in one_line_schedule:
                current_record["rozklady"][line_num].append(line['values'][5]['value'])
        busstops_database.append(current_record)
    return busstops_database
#------------------------------------------------------------------------------
# building bus - line wise schedules database from busstops database
def build_line_schedule_database(busstops_db):
    """
    Function that builds line schedule database using busstops database created by function build_busstops_database

    Parameters:
        busstops_db: database that will be used to create line schedule database

    Returns:
        dict: database of line schedules with line sign (number) as a key and busstop info + schedule as a value
    """

    schedule_database={}
    for records in busstops_db:
        for line, times in records['rozklady'].items():
            if times:
                if line not in schedule_database:
                    schedule_database[line]=[]
                schedule_database[line].append({
                    k: records[k] for k in ['stop_id', 'slupek_id', 'nazwa_zespolu_przyst', 'id_ulicy', 'kierunek', 'Lon', 'Lat']
                })
                schedule_database[line][-1]['rozklad'] = times
    return schedule_database
#------------------------------------------------------------------------------

# building buses database

#------------------------------------------------------------------------------
# function to compute diff between two consecute downloaded buses positions
def compute_dataset_diffs(ds1,ds2):

    """
    Function that computes difference of datasets, based on two datasets obtained from the download_buses_dataset function in the module collect_data

    Parameters:
        ds1: first dataset
        ds2: second dataset

    Returns:
        dict: difference of datasets
    """

    possible_v_max=150
    s1=set(ds1.keys())
    s2=set(ds2.keys())
    s1_intsct_s2 = s1.intersection(s2)

    data_diff={}
    for i in s1_intsct_s2:
        first_timestamp = datetime.strptime(ds1[i]['Time'],'%Y-%m-%d %H:%M:%S')
        second_timestamp = datetime.strptime(ds2[i]['Time'],'%Y-%m-%d %H:%M:%S')
        midtime = first_timestamp + (second_timestamp - first_timestamp)/2
        dt = (second_timestamp-first_timestamp).total_seconds()
        if dt < 1e-6:
            # data_diff[i]=None # sprawdzic poprawnosc
            continue
        d,v = dist_calc_kmh(ds1[i]['Lon'], ds2[i]['Lon'], ds1[i]['Lat'], ds2[i]['Lat'],dt)

        if v > possible_v_max:
            continue

        data_diff[i] = {
            'Line nr': ds1[i]['Lines'],
            'Veh nr': ds1[i]['VehicleNumber'],
            'Lons': [ds1[i]['Lon'], ds2[i]['Lon']],
            'Lats': [ds1[i]['Lat'], ds2[i]['Lat']],
            'Time': midtime.strftime('%Y-%m-%d %H:%M:%S'),
            'Time_diff': dt,
            'distance': d,
            'velocity': v
        }
        data_diff[i]['Lat']= np.mean(data_diff[i]['Lats'])
        data_diff[i]['Lon']= np.mean(data_diff[i]['Lons'])
    return data_diff
#------------------------------------------------------------------------------
# building whole buses database
def build_buses_database(folder_path):
    """
    Function that build buses database based on files obtained by the function download_buses_dataset in the module collect_data,
    processed by a function compute_dataset_diffs

    Parameters:
        folder_path (str): path where you want to save your database
        
    Returns:
        dict: buses database
    """
    buses_db={}
    number_of_set=0
    # try open first dataset
    try:
        with open(os.path.join(folder_path, f"buses_positions_{number_of_set}.json"), "r") as buses_file:
            ds1 = json.load(buses_file)
    except:
        return None
    number_of_set=1
    # open and compute diff for all datasets pairs
    while os.path.isfile(os.path.join(folder_path, f"buses_positions_{number_of_set}.json")):
        # print (number_of_set)
        with open(os.path.join(folder_path, f"buses_positions_{number_of_set}.json"), "r") as buses_file:
            ds2 = json.load(buses_file)
        # compute diff dataset
        dds = compute_dataset_diffs(ds1, ds2)
        # insert computed results to main database of busses
        for k,v in dds.items():
            v['sample'] = number_of_set
            try:
                buses_db[k].append(v)
            except:
                buses_db[k] = [v]
        # prepare ds1 for next loop
        ds1 = ds2
        number_of_set += 1
    return buses_db
