import setuptools

setuptools.setup(
    name="wtplib",
    version="1.0.3",
    author="Adam Ligęza",
    author_email="a.ligeza@mimuw.edu.pl",
    description="Package provides tools for downloading data from https://api.um.warszawa.pl/#, building databases from collected data and for data analysis",
    url="https://gitlab.uw.edu.pl/A.Ligeza/wtplib.git",
    packages=['wtplib'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    install_requires=['numpy', 'sklearn'],
    python_requires='>=3.6',
)