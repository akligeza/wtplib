import unittest
from wtplib import wtputils
from wtplib import building_databases
from wtplib import data_compute

class TestWTPLib(unittest.TestCase):
    def test_disc_calc_kmh1(self):
        res = wtputils.dist_calc_kmh(20.861607,20.868605,52.332905,52.332897,60)
        self.assertAlmostEqual(res,(0.47550214178223016, 28.53012850693381))

    def test_compute_dataset_diffs(self):
        ds0=wtputils.load_data('tests/buses_positions/buses_positions_0.json')
        ds1=wtputils.load_data('tests/buses_positions/buses_positions_1.json')
        res_func = building_databases.compute_dataset_diffs(ds0,ds1)
        res_file = wtputils.load_data('tests/buses_positions_diff.json')
        self.assertEqual(res_func,res_file)
    
    def test_build_buses_database(self):
        res_func = building_databases.build_buses_database('tests/buses_positions')
        res_file = wtputils.load_data('tests/buses_db.json')
        self.assertEqual(res_func,res_file)
    
    def test_build_busstops_database(self):
        res_func = building_databases.build_busstops_database('tests/busstops')
        res_file = wtputils.load_data('tests/busstops_db.json')
        self.assertEqual(res_func,res_file)

    def test_build_line_schedule_database(self):
        busstops_db = wtputils.load_data('tests/busstops_db.json')
        res_func = building_databases.build_line_schedule_database(busstops_db)
        res_file = wtputils.load_data('tests/schedules.json')
        self.assertEqual(res_func,res_file)

    def test_find_bus_groups(self):
        buses_db =  wtputils.load_data('tests/buses_db.json')
        res_func = data_compute.find_bus_groups(buses_db, 400,20, 1)
        res_file = wtputils.load_data('tests/bus_groups_db.json')
        self.assertEqual(res_func,res_file)

    def test_calc_percent_of_overspd(self):
        buses_db =  wtputils.load_data('tests/buses_db.json')
        bus_group = wtputils.load_data('tests/bus_groups_db.json')
        res_func = data_compute.calc_percent_of_overspd(buses_db, bus_group)
        res_file = wtputils.load_data('tests/bus_groups_db_extended.json')
        self.assertEqual(res_func,res_file)

    def test_regions_signif_ovspd(self):
        buses_db =  wtputils.load_data('tests/buses_db.json')
        bus_group = wtputils.load_data('tests/bus_groups_db.json')
        bus_group_ext = data_compute.calc_percent_of_overspd(buses_db, bus_group)
        res_func = data_compute.regions_signif_ovspd(bus_group_ext, 80)
        res_file = wtputils.load_data('tests/bus_groups_db_ext_percent.json')
        self.assertEqual(res_func,res_file)

    def test_avg_busspeed_per_bus(self):
        buses_db =  wtputils.load_data('tests/buses_db.json')
        res_func = data_compute.avg_busspeed_per_bus(buses_db, 40)
        res_file = wtputils.load_data('tests/avg_bus_speed_per_bus.json')
        self.assertEqual(res_func,res_file)

    def test_avg_busspeed(self):
        avg_per_bus = wtputils.load_data('tests/avg_bus_speed_per_bus.json')
        res = data_compute.avg_busspeed(avg_per_bus)
        self.assertAlmostEqual(res,15.597992771397921)

    #def test_bus_on_stop(self):
        #(buses_db, line_schedules, distance_diff: float, max_speed:float)
    def test_overspeed_acts(self):
        buses_db =  wtputils.load_data('tests/buses_db.json')
        res_func = data_compute.overspeed_acts(buses_db, 10)
        res_file = wtputils.load_data('tests/acts_ovspd.json')
        self.assertEqual(res_func,res_file)

    def test_overspeed_buses(self):
        acts_ovspd = wtputils.load_data('tests/acts_ovspd.json')
        res = data_compute.overspeed_buses(acts_ovspd,2)
        self.assertAlmostEqual(res,1)
    
if __name__ == '__main__':
    unittest.main()