# WTPLIB
wtplib is a library for analysing data from Warszawski transport publiczny: https://api.um.warszawa.pl/#
## Features
1) Data downloading and storing them on your HDD (you can download and save raw data or save them in processed form)
2) Database builder module from downloaded data
3) Data computation module
4) File maker for website geojson.io

## Package
You can download this package using `pip install -i https://test.pypi.org/simple wtplib`


## Example of usage
0) Install package
1) import wtplib
2) write wtplib.module_name.function_name eg. '''building_databases.build_buses_database('path to your folder')'''

### Example run of your program

```python
from wtplib import *
```
for data collecting
```python
collect_data.download_buses_dataset('path to folder', APIKEY,3,15,tries_number=60)

collect_data.download_busstops_schedules('path to folder', APIKEY, workers_number=100,tries_number=20, clbk_err=print, clbk_progress=print)
```
for building databases and saving them
```python
busstops_db = building_databases.build_busstops_database('path to folder')
wtputils.save_data(busstops_db,'busstops_db.json')

buses_db = building_databases.build_buses_database('path to folder')
wtputils.save_data(buses_db, 'buses_db.json')

schedules_db = building_databases.build_line_schedule_database(busstops_db)
wtputils.save_data(schedules_db, 'schedules.json')
```
for calculating data
```python
bus_groups = data_compute.find_bus_groups(buses_db, 300, 50, 5)

percent_ovspd = data_compute.calc_percent_of_overspd(buses_db, bus_groups)

regions = data_compute.regions_signif_ovspd(bus_groups, 15)

buses_avg_spd = wdata_compute.avg_busspeed_per_bus(buses_db, 50)

avg = data_compute.avg_busspeed(buses_avg_spd)

punctuality = data_compute.bus_on_stop(buses_db, schedules_db, 50, 20)

punctuality_stats = data_compute.bus_on_stop_stats(punctuality)

ovspd_acts = data_compute.overspeed_acts(buses_db, 50)

ovspd_buses = data_compute.overspeed_buses(ovspd_acts,2)
```


for geojson files generation
```python
drawing_geojson.create_geojson_file_bus_way(buses_db, '8839', 50,'one_line_draw.json')

drawing_geojson.create_geojson_file_rectangle(bus_groups,'rect_where_ovspd.json')
```